A proof-of-concept, self-contained executable that demonstrates the use of CCA attestation in some representative scenarios.

[[_TOC_]]


# Prerequisites

## Veraison Services Setup

All but one PoC depend on a running Veraison services instance that acts as the attestation verifier.

A Veraison cluster needs to be hosted on a networked machine that is reachable from the confidential guest.

By default, the guest is configured to use a pre-provisioned Veraison service hosted at [`veraison.test.linaro.org`](http://veraison.test.linaro.org:8080/.well-known/veraison/verification).    This service is provided for development purposes only as detailed [here](http://veraison.test.linaro.org/disclaimer.html).
If you decide to use it, you can skip the rest of this section as well as the next, and proceed directly to the [commands](#commands) section.

If you do not have connectivity to the Internet, you can run a local Veraison services instance using a ["docker deployment"](https://github.com/veraison/services/blob/main/deployments/docker/README.md).

Docker is a prerequisite.
Installation and post-installation instructions for Ubuntu can be found here:

* [install](https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository)
* [post-install actions](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user)

When docker is correctly installed and configured, issue the following commands to build and start the Veraison service cluster:

```sh
git clone --depth 1 --branch v0.0.2410 https://github.com/veraison/services
cd services
make docker-deploy
```

If everything goes according to plan, the Veraison service cluster is now running.
Source the control functions and explore what you can do:

```sh
source deployments/docker/env.bash

veraison -h
```

Then continue to the [next section](#confidential-guest-setup).

## Confidential Guest Setup

Add the IP address of the machine running Veraison to the guest's `/etc/hosts` file under a `veraison.example` entry.

```sh
guest> su -c 'printf "# veraison services\n__INSERT_VERAISON_PUBLIC_IPADDR_HERE__ veraison.example\n" >> /etc/hosts'
```

By default, the configfs tree (i.e., where the attestation "objects" live) is mounted at `/sys/kernel/config`.
You can check that everything is in order by issuing the following command:

```sh
guest> mount -t configfs
```

If the expected mount point does not show up, execute:

```sh
guest> mount -t configfs none /sys/kernel/config
```

# Commands

## Getting an Attestation Report

The `report` PoC checks that userland can obtain a CCA attestation token from the RMM using the `TSM_REPORT` kernel ABI.

This PoC is completely local to the confidential guest, it does not interact with the Veraison services.

To execute the PoC, do:

```sh
guest> cca-workload-attestation report [-o file.cbor]
```

This will:

1. Request an attestation report for a random challenge[^2] using the `TSM_REPORT` ABI
1. Parse and do syntactic validation on the received CCA token
1. Pretty-print the report claims-set to stdout
1. Pretty-print the report claims-set to stdout and save the raw CBOR to the specified file (default is `cca-token.cbor`)

[^2]: In [RFC9334](https://www.rfc-editor.org/rfc/rfc9334.html#section-10.2) this is called "nonce."


```mermaid
sequenceDiagram
    participant PoC as report
    participant Guest as Guest OS

    PoC->>+Guest: write challenge to<br>/sys/kernel/config/tsm/report/$name/inblob
    Guest->>+RMM: RSI_ATTEST_TOKEN_INIT
    loop
        RMM->>Guest: RSI_INCOMPLETE
        Guest->>RMM: RSI_ATTEST_TOKEN_CONTINUE
    end
    RMM->>-Guest: RSI_SUCCESS
    Guest->>-PoC: read token from<br>/sys/kernel/config/tsm/report/$name/outblob
    PoC-->PoC: parse & pretty print to stdout
```

The output should look like the following[^1]:

[^1]: Explanatory comments have been added to some of the most relevant claims.

<details>
<summary>Click to expand</summary>

```json
{
  "//": "CCA platform claims",
  "cca-platform-token": {
    "cca-platform-challenge": "BwYFBAMCAQAPDg0MCwoJCBcWFRQTEhEQHx4dHBsaGRg=",
    "cca-platform-config": "z8/Pzw==",
    "cca-platform-hash-algo-id": "sha-256",

    "//": "Platform implementation class",
    "cca-platform-implementation-id": "f0VMRgIBAQAAAAAAAAAAAAMAPgABAAAAUFgAAAAAAAA=",

    "//": "Platform implementation instance",
    "cca-platform-instance-id": "AQcGBQQDAgEADw4NDAsKCQgXFhUUExIREB8eHRwbGhkY",
    "cca-platform-lifecycle": 12291,
    "cca-platform-profile": "http://arm.com/CCA-SSD/1.0.0",
    "cca-platform-service-indicator": "https://example.org/.well-known/veraison/verification",
    "cca-platform-sw-components": [
      {
        "measurement-description": "sha-256",
        "measurement-type": "RSE_BL1_2",
        "measurement-value": "micfKpFrC27mzsskJvCzIG7wdFeL5V2byU9vP+Orhqo=",
        "signer-id": "U3h5YwdTXfPsjYsVouLcVkFBnD0wYM/jIjjA+pc/eqM="
      },

      { "//": "Many more measured components have been redacted" },

      {
        "measurement-description": "sha-256",
        "measurement-type": "SOC_FW_CONFIG",
        "measurement-value": "fuKXkfwX6Ya5cSiEViKwd/tF40n9uAUj+snbqHm0rWA=",
        "signer-id": "U3h5YwdTXfPsjYsVouLcVkFBnD0wYM/jIjjA+pc/eqM="
      }
    ]
  },

  "//": "Confidential guest claims",
  "cca-realm-delegated-token": {

    "//": "Submitted challenge",
    "cca-realm-challenge": "1EOBurJYouj+wksH1P7CgKWn71V1YfefslbYZJYPGY6sCV5vsjM9GqbTJJEL+maumCcWBFmrvEH3gYxOsGithg==",

    "//": "Realm extensible measurements - all zero means they haven't been used (yet)",
    "cca-realm-extensible-measurements": [
      "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==",
      "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==",
      "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==",
      "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=="
    ],

    "cca-realm-hash-algo-id": "sha-512",

    "//": "RIM value",
    "cca-realm-initial-measurement": "0hUkD5SwD4d1Y/rAuRo2kewoh4w7jvGap2mJuKo/RMQGIJvrxPit+zPAMiP/NKO6bXG0oCDrTlQSmizmPKJbwg==",

    "//": "Realm personalization value",
    "cca-realm-personalization-value": "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==",

    "cca-realm-public-key": "BHb5iAkb5YXtQYAa7Pq4WFSMYwV+FrDmdhILvQ0vnCngVsXUGgEw65whUXiZ3CMUayjhsGK9PqSzFf0hnxy7Uoy250ykm+Fnc3NPYaHKYQMbK789kY8vlP/EIo5QkZVErg==",
    "cca-realm-public-key-hash-algo-id": "sha-256"
  }
}
```

</details>

## Getting an Attestation Passport

The `passport` PoC runs a remote attestation session with the Veraison verifier.

For this PoC the [Veraison services must be running](#veraison-services-setup).  In particular, the verification endpoint must be reachable from the Guest OS.

To execute the PoC, do:

```sh
cca-workload-attestation passport [-o file.jwt]
```

This will:

1. Open a challenge-response session with a Veraison verifier
1. Request an attestation report for the received challenge using the `TSM_REPORT` ABI
1. Send the obtained CCA attestation token for verification
1. Receive the EAT Attestation Result (EAR) token
1. Pretty-print the EAR claims-set to stdout and save the raw JWT to the specified file (default is `ear.jwt`)

```mermaid
sequenceDiagram
    participant Veraison
    participant PoC as passport
    participant Guest as Guest OS
    participant RMM

    PoC->>Veraison: POST /newSession
    Veraison->>PoC: 201 Created [ { nonce=$challenge } ]
    Note over PoC,RMM: simplified flow
    PoC-->>Guest: challenge
    Guest-->>RMM: challenge
    RMM-->>Guest: token
    Guest-->>PoC: token
    Note over PoC,RMM: see report's sequence diagram for the details
    PoC->>Veraison: POST /session/$sid [ token ]
    Veraison->>PoC: { result: EAR }
    PoC-->PoC: parse & pretty print EAR
    PoC-->PoC: save EAR to file
```

If Veraison has not been configured with platform endorsements (as it is the case here), verification will fail.
The typical output of a failed verification should look like the following[^1]:

<details>
<summary>Click to expand</summary>

```json
{
 "ear.verifier-id": {
  "build": "commit-4f6e556",
  "developer": "Veraison Project"
 },
 "eat_nonce": "jilocWyU_V-27pWu15srAvnvpeXJ1eAZwcfjuIQH3-5W5Z8SebGnrJu1G0dgJ2kpMY7OO-NrAy0ayC--EhCo1Q==",
 "eat_profile": "tag:github.com,2023:veraison/ear",
 "iat": 1714681440,
 "submods": {
  "//": "this appraisal is for the CCA platform using the default policy",
  "CCA_SSD_PLATFORM": {
   "ear.appraisal-policy-id": "policy:CCA_SSD_PLATFORM",

   "//": "A contraindicated status means the verifier has not recognized the attester as trustworthy",
   "ear.status": "contraindicated",

   "//": "the trustworthiness vector contains the details of the appraisal",
   "ear.trustworthiness-vector": {
    "//": "99 is the AR4SI code for failed crypto validation",
    "configuration": 99,
    "executables": 99,
    "file-system": 99,
    "hardware": 99,
    "instance-identity": 99,
    "runtime-opaque": 99,
    "sourced-data": 99,
    "storage-opaque": 99
   },

   "//": "here the default policy could not locate a trust anchor for the supplied token",
   "ear.veraison.policy-claims": {
    "problem": "no trust anchor for evidence"
   }
  }
 }
}
```
</details>

The JWT "passport" is nevertheless saved to file:
```
2024/05/02 20:24:06 EAR passport saved to "ear.jwt"
```

To configure the platform endorsements, please navigate to [this section](#collect-and-configure-reference-values), follow the instructions, and then return to this section.

Re-running the `passport` command:

```sh
cca-workload-attestation passport [-o file.jwt]
```

Should now give an `"affirming"` AR4SI status, meaning that the platform is known and as expected.

The JWT claim-set contains, under the `"ear.veraison.annotated-evidence"` key the whole CCA token in a form that can be easily inspected and evaluated by a policy engine.

<details>
<summary>Click to expand</summary>

```json
{
 "ear.verifier-id": {
  "build": "commit-4f6e556",
  "developer": "Veraison Project"
 },
 "eat_nonce": "a5YFgFUHrxPGXf4wG4xb3OYobLg7GAyFxoJU6xJ4f1BMBFolDD84rNRgrli8BFWoDlTzu6_fvRYfaACuI5asHg==",
 "eat_profile": "tag:github.com,2023:veraison/ear",
 "iat": 1714751841,
 "submods": {
  "CCA_SSD_PLATFORM": {
   "ear.appraisal-policy-id": "policy:CCA_SSD_PLATFORM",
   "ear.status": "affirming",
   "ear.trustworthiness-vector": {
    "configuration": 2,
    "executables": 2,
    "file-system": 0,
    "hardware": 2,
    "instance-identity": 2,
    "runtime-opaque": 2,
    "sourced-data": 0,
    "storage-opaque": 2
   },
   "ear.veraison.annotated-evidence": {
    "platform": {
     "cca-platform-challenge": "tZc8touqn8VVWHhrfsZ/aeQN9bpaqSHNDCf0BYegEeo=",
     "cca-platform-config": "z8/Pzw==",
     "cca-platform-hash-algo-id": "sha-256",
     "cca-platform-implementation-id": "f0VMRgIBAQAAAAAAAAAAAAMAPgABAAAAUFgAAAAAAAA=",
     "cca-platform-instance-id": "AQcGBQQDAgEADw4NDAsKCQgXFhUUExIREB8eHRwbGhkY",
     "cca-platform-lifecycle": 12291,
     "cca-platform-profile": "http://arm.com/CCA-SSD/1.0.0",
     "cca-platform-service-indicator": "https://veraison.example/.well-known/veraison/verification",
     "cca-platform-sw-components": [
      {
       "measurement-description": "sha-256",
       "measurement-type": "RSE_BL1_2",
       "measurement-value": "micfKpFrC27mzsskJvCzIG7wdFeL5V2byU9vP+Orhqo=",
       "signer-id": "U3h5YwdTXfPsjYsVouLcVkFBnD0wYM/jIjjA+pc/eqM="
      },
      {
       "measurement-description": "sha-256",
       "measurement-type": "RSE_BL2",
       "measurement-value": "U8I05ehHK2rFHBrhyrP+BvrQU7646/2Jd7AQZVv908M=",
       "signer-id": "U3h5YwdTXfPsjYsVouLcVkFBnD0wYM/jIjjA+pc/eqM="
      },
      {
       "measurement-description": "sha-256",
       "measurement-type": "RSE_S",
       "measurement-value": "ESHPzNWRPwpj/sQKb/1E6mT53BNcZmNLoAHRC89DAqI=",
       "signer-id": "U3h5YwdTXfPsjYsVouLcVkFBnD0wYM/jIjjA+pc/eqM="
      },
      {
       "measurement-description": "sha-256",
       "measurement-type": "AP_BL1",
       "measurement-value": "FXG17Hi9aFEr94MLtqKkSyBHx99XvOeeuKHA5b6gpQE=",
       "signer-id": "U3h5YwdTXfPsjYsVouLcVkFBnD0wYM/jIjjA+pc/eqM="
      },
      {
       "measurement-description": "sha-256",
       "measurement-type": "AP_BL2",
       "measurement-value": "EBWbryYrQ6ktldtZ2uH3LGRRJzAWYeCjzk44spWpfFg=",
       "signer-id": "U3h5YwdTXfPsjYsVouLcVkFBnD0wYM/jIjjA+pc/eqM="
      },
      {
       "measurement-description": "sha-256",
       "measurement-type": "SCP_BL1",
       "measurement-value": "EBIuhWs/zUnwY2NjF0dhSctzChqhz6rYGFUrcvVtb2g=",
       "signer-id": "U3h5YwdTXfPsjYsVouLcVkFBnD0wYM/jIjjA+pc/eqM="
      },
      {
       "measurement-description": "sha-256",
       "measurement-type": "SCP_BL2",
       "measurement-value": "qmehabC7oheqCqiKZTRpIMhMQkR8NrpffqZfQiwf5dg=",
       "signer-id": "8UtJh5BLy1gU5EWaBX7U0g9YpjMVIoinYSFNzSh4C1Y="
      },
      {
       "measurement-description": "sha-256",
       "measurement-type": "AP_BL31",
       "measurement-value": "Lm0xpZg6kSUb+uWu+hwKGdi6PPYB0OinBrTPqWYaa4o=",
       "signer-id": "U3h5YwdTXfPsjYsVouLcVkFBnD0wYM/jIjjA+pc/eqM="
      },
      {
       "measurement-description": "sha-256",
       "measurement-type": "RMM",
       "measurement-value": "oftQ5shvrhZ57zNRKW/WcTQRoIz43ReQpP0F+uhogWQ=",
       "signer-id": "U3h5YwdTXfPsjYsVouLcVkFBnD0wYM/jIjjA+pc/eqM="
      },
      {
       "measurement-description": "sha-256",
       "measurement-type": "HW_CONFIG",
       "measurement-value": "GiUkApcvYFf6U8wXK1K5/8ppjhgxH6zQ87Buyq73nhc=",
       "signer-id": "U3h5YwdTXfPsjYsVouLcVkFBnD0wYM/jIjjA+pc/eqM="
      },
      {
       "measurement-description": "sha-256",
       "measurement-type": "FW_CONFIG",
       "measurement-value": "mpKtvAzuOO9ljHHOGxv4xlZo8Wa/shNkTIlcyxrQeiU=",
       "signer-id": "U3h5YwdTXfPsjYsVouLcVkFBnD0wYM/jIjjA+pc/eqM="
      },
      {
       "measurement-description": "sha-256",
       "measurement-type": "TB_FW_CONFIG",
       "measurement-value": "I4kDGAzBBOwsXYs/IMW8YbOJ7AqWffjMIIzcfNRUF08=",
       "signer-id": "U3h5YwdTXfPsjYsVouLcVkFBnD0wYM/jIjjA+pc/eqM="
      },
      {
       "measurement-description": "sha-256",
       "measurement-type": "SOC_FW_CONFIG",
       "measurement-value": "5sIejSYP5xiC3r2zOdJAKiynZIUpvCMD9IZJvOA4ABc=",
       "signer-id": "U3h5YwdTXfPsjYsVouLcVkFBnD0wYM/jIjjA+pc/eqM="
      }
     ]
    },
    "realm": {
     "cca-realm-challenge": "a5YFgFUHrxPGXf4wG4xb3OYobLg7GAyFxoJU6xJ4f1BMBFolDD84rNRgrli8BFWoDlTzu6/fvRYfaACuI5asHg==",
     "cca-realm-extensible-measurements": [
      "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==",
      "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==",
      "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==",
      "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=="
     ],
     "cca-realm-hash-algo-id": "sha-512",
     "cca-realm-initial-measurement": "rof79/EF5xp4J8tvCJaInHPSvq9IMNT5ywXj6Ww/x7T6oEyvNqJr0JTPnHtzhhfkwYmq7hhN7dpdegPDnb2sfA==",
     "cca-realm-personalization-value": "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==",
     "cca-realm-public-key": "BHb5iAkb5YXtQYAa7Pq4WFSMYwV+FrDmdhILvQ0vnCngVsXUGgEw65whUXiZ3CMUayjhsGK9PqSzFf0hnxy7Uoy250ykm+Fnc3NPYaHKYQMbK789kY8vlP/EIo5QkZVErg==",
     "cca-realm-public-key-hash-algo-id": "sha-256"
    }
   }
  }
 }
}
```

</details>

## Configure Reference Values

To configure Veraison services with the needed endorsements, clone the `poc-endorser` repository:

```sh
git clone https://git.codelinaro.org/linaro/dcap/cca-demos/poc-endorser.git
```

and follow the instructions in [README.md](https://git.codelinaro.org/linaro/dcap/cca-demos/poc-endorser/-/blob/master/README.md)

## Future PoCs

- Getting an X.509 Certificate by means of an [attested CSR](https://datatracker.ietf.org/doc/draft-ietf-lamps-csr-attestation/);
- Establishing an [attested TLS](https://datatracker.ietf.org/doc/draft-fossati-tls-attestation) Session.

# Manual Build and Installation

> **Note:** The instructions in this section are only relevant to a developer.
> Under normal circumstances, the `cca-workload-attestation` executable is automatically available in the confidential guest's interactive shell.
> Please refer to [this documentation](https://linaro.atlassian.net/wiki/spaces/QEMU/pages/29051027459/Building+an+RME+stack+for+QEMU) for more details on the CCA emulation environment.

To create a static, cross-compiled executable for `aarch64/linux`, do:

```sh
CGO_ENABLED=0 GOARCH=arm64 GOOS=linux go build -ldflags="-s -w" -o cca-workload-attestation
```

To deploy the executable to the confidential guest, mount a shared folder in the guest and manually install it:

```sh
guest> mount -t 9p shr0 /mnt
# copy the executable to the shared folder
guest> install -m 555 /mnt/cca-workload-attestation /usr/bin
```

