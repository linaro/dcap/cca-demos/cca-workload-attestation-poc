package main

import (
	"crypto/rand"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"time"

	"github.com/google/go-configfs-tsm/configfs/linuxtsm"
	tsm "github.com/google/go-configfs-tsm/report"
	"github.com/lestrrat-go/jwx/v2/jwa"
	"github.com/lestrrat-go/jwx/v2/jwk"
	"github.com/veraison/apiclient/common"
	"github.com/veraison/apiclient/verification"
	"github.com/veraison/ccatoken"
	"github.com/veraison/ear"
)

const (
	WellKnownPath      = "/.well-known/veraison/verification"
	WellKnownMediaType = "application/vnd.veraison.discovery+json"
	Timeout            = 10 * time.Second
	DefaultHost        = "veraison.example:8443"
)

func main() {
	passportCmd := flag.NewFlagSet("passport", flag.ExitOnError)
	passportName := passportCmd.String("o", "ear.jwt", "file where the EAR passport is saved")

	reportCmd := flag.NewFlagSet("report", flag.ExitOnError)
	reportName := reportCmd.String("o", "cca-token.cbor", "file where the CCA token is saved")

	_ = flag.NewFlagSet("golden", flag.ExitOnError)

	help := "Available subcommands: 'passport', 'report', 'golden (WIP)'"

	if len(os.Args) < 2 {
		fmt.Fprintln(os.Stderr, help)
		os.Exit(0)
	}

	switch os.Args[1] {
	case "passport":
		if err := passportCmd.Parse(os.Args[2:]); err == nil {
			passport(*passportName)
		}
	case "report":
		if err := reportCmd.Parse(os.Args[2:]); err == nil {
			report(*reportName)
		}
	case "golden":
		golden()
	default:
		fmt.Fprintln(os.Stderr, help)
		os.Exit(0)
	}
}

// Select a suitable HTTP or HTTPS client
func NewConfig() (*verification.ChallengeResponseConfig, error) {
	client := common.NewClient(nil)
	client.HTTPClient.Timeout = Timeout
	uri := &url.URL{
		Scheme: "http",
		Host:   DefaultHost,
		Path:   WellKnownPath,
	}

	res, err := client.GetResource(WellKnownMediaType, uri.String())
	if err != nil {
		return nil, err
	}
	// The server could be HTTPS-only
	if res.StatusCode == http.StatusBadRequest {
		client = common.NewInsecureTLSClient(nil)
		client.HTTPClient.Timeout = Timeout
		uri.Scheme = "https"

		res, err = client.GetResource(WellKnownMediaType, uri.String())
		if err != nil {
			return nil, err
		}
	}

	if res.StatusCode != http.StatusOK {
		return nil, fmt.Errorf(".well-known query returned status %s", res.Status)
	}

	var verificationService struct {
		APIEndpoints struct {
			NewChallengeResponseSession string `json:"newChallengeResponseSession"`
		} `json:"api-endpoints"`
	}

	err = common.DecodeJSONBody(res, &verificationService)
	if err != nil {
		return nil, err
	}

	uri.Path = verificationService.APIEndpoints.NewChallengeResponseSession

	return &verification.ChallengeResponseConfig{
		NonceSz:         64,
		EvidenceBuilder: TSMEvidenceBuilder{},
		NewSessionURI:   uri.String(),
		DeleteSession:   true,
		Client:          client,
		UseTLS:          uri.Scheme == "https",
		IsInsecure:      true,
	}, nil
}

func passport(out string) {
	cfg, err := NewConfig()
	if err != nil {
		log.Fatalf("Could not create challenge-response config: %v", err)
	}

	ar, err := cfg.Run()
	if err != nil {
		log.Fatalf("Veraison API client session failed: %v", err)
	}

	jwtString := ar[1 : len(ar)-1]

	if err := processEAR(jwtString); err != nil {
		log.Fatalf("EAR processing failed: %v", err)
	}

	if err = os.WriteFile(out, jwtString, 0644); err != nil {
		log.Fatalf("Saving EAR passport to %q failed: %v", out, err)
	}

	log.Printf("EAR passport saved to %q", out)
}

func processEAR(ares []byte) error {
	earVerificationKey := `{
		"alg": "ES256",
		"crv": "P-256",
		"kty": "EC",
		"x": "usWxHK2PmfnHKwXPS54m0kTcGJ90UiglWiGahtagnv8",
		"y": "IBOL-C3BttVivg-lSreASjpkttcsz-1rb7btKLv8EX4"
	}`

	vfyK, _ := jwk.ParseKey([]byte(earVerificationKey))

	var ar ear.AttestationResult

	if err := ar.Verify(ares, jwa.ES256, vfyK); err != nil {
		return err
	}

	j, _ := ar.MarshalJSONIndent("", " ")
	fmt.Println(string(j))

	return nil
}

type TSMEvidenceBuilder struct{}

func (eb TSMEvidenceBuilder) BuildEvidence(nonce []byte, accept []string) ([]byte, string, error) {
	for _, ct := range accept {
		if ct == `application/eat-collection; profile="http://arm.com/CCA-SSD/1.0.0"` {
			evidence, err := getEvidence(nonce)
			if err != nil {
				return nil, "", err
			}

			return evidence, ct, nil
		}
	}

	return nil, "", errors.New("no match on accepted media types")
}

func golden() {
	cbor, err := getEvidence(getRandomNonce())
	if err != nil {
		log.Fatalf("getEvidence failed: %v", err)
	}

	evidence, err := ccatoken.DecodeAndValidateEvidenceFromCBOR(cbor)
	if err != nil {
		log.Fatalf("Parsing CCA evidence from CBOR failed: %v", err)
	}

	instID := evidence.GetInstanceID()
	log.Printf("(TODO) Instance ID: %x\n", *instID)
}

func report(out string) {
	cbor, err := getEvidence(getRandomNonce())
	if err != nil {
		log.Fatalf("getEvidence failed: %v", err)
	}

	evidence, err := ccatoken.DecodeAndValidateEvidenceFromCBOR(cbor)
	if err != nil {
		log.Fatalf("Parsing CCA evidence from CBOR failed: %v", err)
	}

	s, err := prettyPrintToken(evidence)
	if err != nil {
		log.Fatalf("Reformatting CCA evidence failed: %v", err)
	}

	fmt.Println(s)

	if err = os.WriteFile(out, cbor, 0644); err != nil {
		log.Fatalf("Saving CCA token to %q failed: %v", out, err)
	}

	log.Printf("CCA token saved to %q", out)
}

func prettyPrintToken(e *ccatoken.Evidence) (string, error) {
	j, err := e.MarshalJSON()
	if err != nil {
		return "", fmt.Errorf("serializing CCA claims-set to JSON failed: %w", err)
	}

	i := make(map[string]interface{})

	err = json.Unmarshal(j, &i)
	if err != nil {
		return "", fmt.Errorf("deserializing CCA claims-set from JSON failed: %w", err)
	}

	b, err := json.MarshalIndent(i, "", "  ")
	if err != nil {
		return "", fmt.Errorf("re-marshaling CCA token failed: %w", err)
	}

	return string(b), nil
}

func getEvidence(nonce []byte) ([]byte, error) {
	req := &tsm.Request{
		InBlob: nonce,
	}

	res, err := linuxtsm.GetReport(req)
	if err != nil {
		return nil, fmt.Errorf("GetReport failed: %w", err)
	}

	return res.OutBlob, nil
}

func getRandomNonce() []byte {
	nonce := make([]byte, 64)
	rand.Read(nonce)
	return nonce
}
